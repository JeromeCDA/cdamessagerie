package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Utilisateur")
@NamedQuery(name = "findByLogin", query = "select user from Utilisateur user where user.login = :login") 

public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_gen")
	@SequenceGenerator(name="user_id_gen", sequenceName = "seq_user", allocationSize = 1)
	@Column (name = "idUser")
	private int idUser;
	@Column(name = "Nom")
	private String nom;
	@Column(name = "Prenom")
	private String prenom;
	@Column(name = "Mail", unique = true)
	private String mail;
	@Column(name = "Telephone")
	private String telephone;
	@Column(name = "Login", unique = true)
	private String login;
	@Column(name = "Password")
	private String password;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
//	@JoinColumn(name = "idMail")
	private List <Mail> ListMail;
	
	
	public Utilisateur(String nom, String prenom, String mail, String telephone, String login, String password) {
		
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = telephone;
		this.login = login;
		this.password = password;
				
	}
	
	public Utilisateur(int idUser, String nom, String prenom, String mail) {
		
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
						
	}
		
	public Utilisateur(int idUser) {
		
		this.idUser = idUser;
			
	}
	
}
