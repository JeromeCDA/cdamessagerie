package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name = "Mail")
@NamedQuery(name = "findById", query = "select mail from Mail mail where user.idUser = :iduser") 
public class Mail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "email_id_gen")
	@SequenceGenerator(name="email_id_gen", sequenceName = "seq_email", allocationSize = 1)
	@Column (name = "idMail")
	int idMail;
	@Column(name = "texteMail")
	String texteMail;
	@Column(name = "objetMail")
	String objetMail;
	@Column(name = "mailDestinataire")
	String mailDestinataire;
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn (name = "idUser")
	private Utilisateur user;
	

	public Mail(String texteMail, String objetMail, 
				String mailDestinataire) {
		
		this.texteMail = texteMail;
		this.objetMail = objetMail;
		this.mailDestinataire = mailDestinataire;
	}
	
	@Override
	public String toString() {
 
	    return mailDestinataire+"     "+texteMail+"     "+objetMail;
	}

}
