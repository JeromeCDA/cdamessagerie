package fr.afpa.model;

import fr.afpa.beans.Utilisateur;

import fr.afpa.dao.communicationBdd;
import fr.afpa.view.UiAccueil;
import fr.afpa.view.UiAuthentification;
import fr.afpa.view.UiNouveauMessage;
import fr.afpa.beans.Mail;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.communicationBdd;

public class InstanceUser {
	
	private static String nomUser;
	private static String prenomUser;
	private static String mailUser;
	private static int idUser;
	static Utilisateur loggedUser;

	public void CreateUser(String login, String password) {



	}
	
	
	/**
	 * @author selim
	 * Methode permettant l'ouverture de la page d'accueil une fois les identifiants validés par la couche DAO
	 */
	public UiAccueil CheckUser(String login, String password) {

		communicationBdd commBD = new communicationBdd();

		if (commBD.checkValidUser(login, password) == true) {
			
			UiAccueil accueilUser = new UiAccueil();
			System.out.println("Authentification réussie");
			return accueilUser;
		}
		
		else  {UiAuthentification.incorrectLoginOrPassword();}
		return null;
	}
	
	
	/**
	 * @author selim
	 * methode permettant d'instancier un utilisateur à partir de son login et des informations contenues dans la base de données
	 * @param login
	 * @return
	 */
	public static Utilisateur recupererUser(String login) {
		
		nomUser = communicationBdd.nomUser(login);
		prenomUser = communicationBdd.prenomUser(login);
		mailUser = communicationBdd.mailUser(login);
		idUser = communicationBdd.idUser(login);
		
		loggedUser = new Utilisateur(idUser, nomUser, prenomUser, mailUser);
		
		return loggedUser;
		
	}

	public void retrieveLoginMail(String login, String mail) {
		
		
		
	}
	
	
	/**@author Djé
	 * Instanciation d'un nouvel utilisateur. La clé étrangère de l'user étant dans la table mail, la méthode DAO n'a besoin que de l'objet email qui contient toutes les infos de l'user
	 * @param nom
	 * @param prenom
	 * @param login
	 * @param mail
	 * @param telNum
	 * @param password
	 */
	public void userInstanciation(String nom, String prenom, String login, String mail, String telNum, String password) {
		
		communicationBdd bdd = new communicationBdd();
		
		Utilisateur user = new Utilisateur();
		Mail email = new Mail();
		
		user.setLogin(login);
		user.setMail(mail);
		user.setNom(nom);
		user.setPassword(password);
		user.setPrenom(prenom);
		user.setTelephone(telNum);
		
		email.setUser(user);
		
		bdd.createUserSQL(email);
	}
}
