package fr.afpa.model;

import fr.afpa.beans.Mail;
import fr.afpa.dao.communicationBdd;

public class InstanceMessage {
	
	Mail mailDB = new Mail();
	
	/**
	 * @author selim
	 * methode permettant d'inistancier un objet mail et de l'envoyer vers la couche DAO
	 */
	public void EcrireMessage ( String texte, String objet, String destinataire) {
		
		mailDB = new Mail(texte, objet, destinataire);
		
		communicationBdd commBD = new communicationBdd();
		
		commBD.insertIntoDB(mailDB);
	}
}
