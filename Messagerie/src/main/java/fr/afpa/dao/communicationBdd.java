package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Mail;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.InstanceUser;
import fr.afpa.session.HibernateUtils;
import lombok.Getter;
import lombok.ToString;

public class communicationBdd {

	Mail mailDB;
	Utilisateur user;
	public static Utilisateur loggedUser;
	public static String loginUser;
	static private String nom = "";
	static private String prenom = "";
	static private String mail = "";
	private String lectureMail ;
	static private int idUser;
	private static ArrayList <Mail> listeMailRecherche;
	
	
	public void selectTextMail() {

	}

	public void selectObjetMail() {

	}

	public void selectNomDestinataire() {

	}

	public void createUserSQL() {

	}
	
	
	/**
	 * @author selim
	 * @param 
	 * enregistrement du mail rédigé par l'utilisateur dans la base de données
	 */
	public void insertIntoDB(Mail mailDB) {

		user = InstanceUser.recupererUser(loginUser);
				
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		mailDB.setUser(user);
		
		s.save(mailDB);
		tx.commit();
		s.close();

	}

	
	/**
	 * @author selim
	 * @param
	 *     fonction booleenne verifiant la correspondance entre le login et le
	 *     password de l'utilisateur la fonction renvoie "true" si le
	 *     mot de passe de l'utilisateur correspond à celui renseigné
	 *     dans l'interface. Le login est enregistré dans la variable static 'login' 
	 *     et peut être récupéré via le getter correspondant
	 * @param texte
	 */
	public boolean checkValidUser(String login, String password) {

		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();

		Query qLogin = s.getNamedQuery("findByLogin");
		qLogin.setParameter("login", login);

		ArrayList<Utilisateur> listeLoginUser = (ArrayList<Utilisateur>) qLogin.getResultList();

		for (Utilisateur user : listeLoginUser) {

			if (user.getPassword().equals(password)) {
				
				//instanciation et recuperation du client authentifié
				loginUser = login;
				loggedUser = InstanceUser.recupererUser(login);
				s.close();
				return true;
			}
		}
		s.close();
		return false;
	}
	
	
	public void retrieveLoginMailUnique () {
		
		Session sess = HibernateUtils.getSession();
		Transaction tx = sess.beginTransaction();
		
    	Query q = sess.getNamedQuery("findByNom");
   
    	ArrayList <Utilisateur> listePersonnes = (ArrayList <Utilisateur>) q.getResultList();
    		for(Utilisateur personne:listePersonnes) {
    			System.out.println("Logins + Mails : "+personne.getLogin()+"\n");
    			System.out.println(personne.getMail()+"\n");
    		}	
	}
	
	
	/**@author Djé
	 * Méthode DAO qui permet l'enregistrement dans la DB d'un utilisateur
	 * @param email
	 */
	public void createUserSQL(Mail email) {
		
		Session sess = HibernateUtils.getSession();
		Transaction tx = sess.beginTransaction();
		
		sess.save(email);
		tx.commit();
		sess.close();
	}
	
	
	/**
	 * @author selim
	 * @param login 
	 * methode permettant, pour un login donné, de récuperer le nom de l'utilisateur
	 * @return
	 */
	public static String nomUser(String login) {

		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();

		Query qLogin = s.getNamedQuery("findByLogin");
		qLogin.setParameter("login", login);

		ArrayList<Utilisateur> listeLoginUser = (ArrayList<Utilisateur>) qLogin.getResultList();

		for (Utilisateur user : listeLoginUser) {

			nom = "" + user.getNom();
			s.close();
		}

		return nom;
	}

	
	/**
	 * @author selim
	 * @param login 
	 * methode permettant, pour un login donné, de récuperer le prenom de l'utilisateur
	 * @return
	 */
	public static String prenomUser(String login) {

		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();

		Query qLogin = s.getNamedQuery("findByLogin");
		qLogin.setParameter("login", login);

		ArrayList<Utilisateur> listeLoginUser = (ArrayList<Utilisateur>) qLogin.getResultList();

		for (Utilisateur user : listeLoginUser) {

			prenom = "" + user.getPrenom();
			s.close();
		}

		return prenom;
	}
	
	/**
	 * @author selim
	 * @param login 
	 * methode permettant, pour un login donné, de récuperer le mail de l'utilisateur
	 * @return
	 */
	public static String mailUser(String login) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		Query qLogin = s.getNamedQuery("findByLogin");
		qLogin.setParameter("login", login);
		
		ArrayList<Utilisateur> listeLoginUser = (ArrayList<Utilisateur>) qLogin.getResultList();
		
		for (Utilisateur user : listeLoginUser) {
			
			mail = "" + user.getMail();
			s.close();
		}
		
		return mail;
	}
	
	/**
	 * @author selim
	 * @param login 
	 * methode permettant, pour un login donné, de récuperer l'id de l'utilisateur
	 * @return
	 */
	public static int idUser(String login) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		Query qLogin = s.getNamedQuery("findByLogin");
		qLogin.setParameter("login", login);
		
		ArrayList<Utilisateur> listeLoginUser = (ArrayList<Utilisateur>) qLogin.getResultList();
		
		for (Utilisateur user : listeLoginUser) {
			
			idUser = user.getIdUser();
			s.close();
		}
		
		return idUser;
	}
	
	/**
	 * @author selim
	 * methode de recupération des emetteurs, objets et textes des mails reçus par l'utilisateur
	 */
	public static ArrayList <Mail> listeMail (String entry) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		Query qUserId = s.getNamedQuery("findById");
		qUserId.setParameter("iduser", loggedUser.getIdUser());
		
		ArrayList <Mail> listeMailUser = (ArrayList<Mail>) qUserId.getResultList();
		listeMailRecherche = new ArrayList <Mail> ();
		
		
		for (Mail mail : listeMailUser) {	
			
			if (mail.getTexteMail().contains(entry) || mail.getUser().getMail().contains(entry) || mail.getObjetMail().contains(entry)){
				
				listeMailRecherche.add(mail);
//				s.close();
//				return listeMailRecherche;
			}
		}
					
		return listeMailRecherche;
	}

}