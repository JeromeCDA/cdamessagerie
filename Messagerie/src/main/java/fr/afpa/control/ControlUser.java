package fr.afpa.control;


import javax.persistence.Query;
import javax.swing.JLabel;

import fr.afpa.dao.communicationBdd;
import fr.afpa.model.InstanceMessage;
import fr.afpa.model.InstanceUser;
import fr.afpa.view.UiAccueil;
import fr.afpa.view.UiAuthentification;

import java.sql.SQLIntegrityConstraintViolationException;

import fr.afpa.model.InstanceUser;
import fr.afpa.view.UiCreationCompte;
import fr.afpa.view.UiRecherche;
import fr.afpa.view.UiVisuelMessage;


public class ControlUser {
	

	/**
	 * @author selim
	 * METHODE DE CONTROLE D'INSERTION DE DONNEES DANS LES CHAMPS DE LA FENETRE D'AUTHENTIFICATION
	 */
	public boolean controlValidUser(String login, String password) {

		if (!login.contentEquals("") && !password.contentEquals("")) {
			
			InstanceUser newUser = new InstanceUser();
			newUser.CheckUser(login, password);
			//Appelle la frame main menu si login ok, pas besoin de pop-up donc switch direct
			
		}
		
		else {System.out.println("Problème d'authentification controlUser");
		UiAuthentification.incorrectLoginOrPassword();
		UiAuthentification.emptyFields();}
		return false;
	}
	
		


	
	/**@author Djé
	 * Doit contenir 8 caractères minimum chiffres ou lettres, utilisation de regex pour imposer un format
	 * @param userLogin
	 * @return
	 */
	public boolean controlPasswordInscription(String userLogin) {
		boolean verif = true;
		String regex = "([a-z]|[0-9]){8}";
		
		if (!userLogin.matches(regex)) {
			verif = false;
		} else { System.out.println(userLogin);
		}
		return verif;
	}
	
	
	/**@author Djé
	 * Tous les champs doivent être remplis
	 * @param nom
	 * @param prenom
	 * @param login
	 * @param mail
	 * @param telNum
	 * @param password
	 * @return
	 */
	public boolean controlEmptyFields(String nom, String prenom, String login, String mail, String telNum, String password) {
		boolean verif = true;
		if ("".equals(nom) || "".equals(prenom) || "".equals(login) || "".equals(mail) || "".equals(telNum) || "".equals(password)) {
			verif = false;
		}
		return verif;		
	}
	
	
	/**@author Djé
	 * Méthode générale de contrôle de la classe UiCreationCompte qui appelle tous les contrôles. Permet également grâce à la variable booleenne verif d'appeler la méthode UI switchToAuthentification()
	 * si tout a réussi, et ainsi de passer à la frame suivante
	 * @param nom
	 * @param prenom
	 * @param login
	 * @param mail
	 * @param telNum
	 * @param password
	 */
	public void controlOkForInstanceUser(String nom, String prenom, String login, String mail, String telNum, String password) {
		UiCreationCompte crea = new UiCreationCompte();
		InstanceUser iu = new InstanceUser();
		boolean verif = true;

		if (!controlEmptyFields(nom, prenom, login, mail, telNum, password)){
			crea.emptyFields();
			verif = false;
			return;
		}
		if (!controlPasswordInscription(password)) {
			crea.passwordWrongFormat();
			verif = false;
			return;
		}
		try {
			iu.userInstanciation(nom, prenom, login, mail, telNum, password);
		} catch (Exception e) {
			crea.sqlUniqueException();
			verif = false;
			if (e instanceof SQLIntegrityConstraintViolationException) {
				crea.sqlUniqueException();
			}
		}
		if (verif) {
			crea.switchToAuthentification();
		}
	}
	
	
	
	public boolean controlConnexionPassword() {
		
		
		return false;
	}
	
	
	public boolean controlExistenceUserUnique() {
		
		
		return false;
	}
	

	
	
}
