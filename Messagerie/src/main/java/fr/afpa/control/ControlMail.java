package fr.afpa.control;

import fr.afpa.model.InstanceMessage;
import fr.afpa.view.UiNouveauMessage;

public class ControlMail {

	
	public boolean mailNonLu() {
		
		return false;
	}
	
	
	public boolean nombreMailRecu() {
		
		return false;
	}
	
	
	public boolean controlObjetMail() {
		
		return false;
	}
	
	
	public boolean controlDestinataire() {
		
		return false;
	}

	
	/**
	 * @author selim
	 * METHODE DE CONTROLE D'INSERTION DE DONNEES DANS LES CHAMPS DE LA FENETRE "NOUVEAU MESSAGE"
	 */
	public boolean controlEnvoiMail(String destinataire, String objet, String texte) {

		if (destinataire.contentEquals("") || objet.contentEquals("") || texte.contentEquals("")) {

			erreurControl();
			return false;
		}

		InstanceMessage instMessage = new InstanceMessage();
		instMessage.EcrireMessage(destinataire, objet, texte);
		UiNouveauMessage newMail = new UiNouveauMessage();
		newMail.switchToMainMenu();
		return true;
	}
	
	/**
	 * @author selim
	 * lOG D'ERREUR
	 */
	public static void erreurControl() {
		
		System.out.println("Veuillez renseigner les champs vides");
	}
	
}
