package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.afpa.control.ControlMail;

public class UiNouveauMessage extends JFrame implements ActionListener, WindowListener, TextListener, MouseListener{
	
	private JTextField destinataire, objet;
	private JLabel labDestinataire, labObjet;
	private JTextArea texteMessage;
	private JButton envoyer;
	private JMenuBar menuBar;
	private JMenu fichier, menuhelp;
	private JMenuItem newMessage, deconnexion;
	private Dimension longSize , sideSize;
	
	/**
	 * @AUTHOR selim
	 * METHODE CREATION NOUVEAU MESSAGE
	 */
	public UiNouveauMessage() {
		
		super("CDA Messagerie");
		
		// create Layout
		setLayout(new BorderLayout());
		addWindowListener(this);
		
		//CREATE MENUBAR
		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		fichier = new JMenu("Fichier");
		fichier.setBackground(Color.WHITE);
		menuhelp = new JMenu("?");
		menuhelp.setBackground(Color.WHITE);
		newMessage = new JMenuItem("Nouveau message");
		newMessage.setBackground(Color.WHITE);
		newMessage.addMouseListener(this);
		deconnexion = new JMenuItem("Deconnexion");
		deconnexion.setBackground(Color.WHITE);
		deconnexion.addMouseListener(this);

		fichier.add(newMessage);
		fichier.add(deconnexion);
		menuBar.add(fichier);
		menuBar.add(menuhelp);
		setJMenuBar(menuBar);
				
		//CREATE BORDER AND CENTRAL PANEL
		longSize = new Dimension (700, 30);
		sideSize = new Dimension (30, 400);
		
		JPanel northSpace = new JPanel();
		northSpace.setBackground(Color.WHITE);
		northSpace.setPreferredSize(longSize);
		
		JPanel westSpace = new JPanel();
		westSpace.setBackground(Color.WHITE);
		westSpace.setPreferredSize(sideSize);
		
		JPanel eastSpace = new JPanel();
		eastSpace.setBackground(Color.WHITE);
		eastSpace.setPreferredSize(sideSize);
		
		JPanel southSpace = new JPanel();
		southSpace.setBackground(Color.WHITE);
		southSpace.setPreferredSize(longSize);
		
		JPanel central = new JPanel();
		central.setBackground(Color.WHITE);
		central.setLayout(new BorderLayout());
		
		this.add(northSpace, BorderLayout.NORTH);
		this.add(westSpace, BorderLayout.WEST);
		this.add(eastSpace, BorderLayout.EAST);
		this.add(southSpace, BorderLayout.SOUTH);
		this.add(central, BorderLayout.CENTER);
		
		
		JPanel centralNorth = new JPanel();
		centralNorth.setBackground(Color.WHITE);
		JPanel centralSouth = new JPanel();
		centralSouth.setBackground(Color.WHITE);
		JPanel centralNorth1 = new JPanel();
		centralNorth1.setBackground(Color.WHITE);
		JPanel centralNorth2 = new JPanel();
		centralNorth2.setBackground(Color.WHITE);
		
		centralNorth.setLayout(new GridLayout(2,1));
		centralSouth.setLayout(new GridLayout(1,4));
		centralNorth1.setLayout(new FlowLayout());
		centralNorth2.setLayout(new FlowLayout());
				
		TitledBorder border= BorderFactory.createTitledBorder("Nouveau message");
		border.setTitleColor(Color.BLACK);
		border.setTitleFont(new Font("Bahnschrift", Font.ROMAN_BASELINE, 15));;
		central.setBorder(border);
		
		
		//CREATE TEXTFIELD, TEXTAREA, LABEL
		labDestinataire = new JLabel(" A   : ");
		labDestinataire.setPreferredSize(new Dimension(80, 20));
		labObjet = new JLabel(" Objet   : ");
		labObjet.setPreferredSize(new Dimension(80, 20));
		destinataire = new JTextField();
		destinataire.setEditable(true);
		destinataire.setPreferredSize(new Dimension(500, 20));
		objet = new JTextField();
		objet.setEditable(true);
		objet.setPreferredSize(new Dimension(500, 20));
		
		Border borderTextArea = BorderFactory.createLineBorder(Color.LIGHT_GRAY);
		texteMessage = new JTextArea();
		texteMessage.setBorder(borderTextArea);
		texteMessage.setEditable(true);
		
		centralNorth1.add(labDestinataire);
		centralNorth1.add(destinataire);
		centralNorth.add(centralNorth1);
		
		centralNorth2.add(labObjet);
		centralNorth2.add(objet);
		centralNorth.add(centralNorth2);
		
		central.add(centralNorth, BorderLayout.NORTH);
		central.add(texteMessage, BorderLayout.CENTER);
		
		envoyer = new JButton("Envoyer");
		envoyer.setName("envoyer");
		envoyer.addMouseListener(this);
		centralSouth.add(envoyer);
		centralSouth.add(Box.createHorizontalGlue());
		centralSouth.add(Box.createHorizontalGlue());
		centralSouth.add(Box.createHorizontalGlue());
		
				
		central.add(centralSouth, BorderLayout.SOUTH);
			
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(700, 400);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setVisible(true);
		
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println(((JButton)e.getSource()).getName());
		
		ControlMail control = new ControlMail();
		
		if (((JButton)e.getSource()).getName().contentEquals("envoyer")) {
			
			control.controlEnvoiMail(destinataire.getText(), objet.getText(), texteMessage.getText());
		}
		
		else { System.out.println("Probleme d'envoi");}
	}
	
	
	/**@author Djé
	 * Méthode qui permet de reroot vers le main menu si le message est bien envoyé
	 */
	public void switchToMainMenu() {
		this.setVisible(false);
		JFrame f = new JFrame();
		Object[] options = {"Retour à la page d'accueil"};
		int option = JOptionPane.showOptionDialog(null,
                "Message envoyé",
                "Envoie de message : ",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
		if (option == JOptionPane.OK_OPTION) {
			//appelle la frame de la page d'accueil si le message est bien envoyé
			UiAccueil mainMenu = new UiAccueil();
		} 
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
