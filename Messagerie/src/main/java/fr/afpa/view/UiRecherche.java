package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.afpa.beans.Mail;
import fr.afpa.beans.Utilisateur;
import fr.afpa.control.ControlMail;
import fr.afpa.dao.communicationBdd;
import fr.afpa.model.InstanceUser;

public class UiRecherche extends JFrame implements ActionListener, WindowListener, TextListener, MouseListener {
	
	private JPanel northSpace, eastSpace, southSpace, westSpace, central, centralNorth, centralSouth, disconnectLine, searchLine;
	private String nomUser, prenomUser;
	private JLabel welcome, accueil, deconnexion, search;
	private JTextField rechercher;
	private JList listeMail;
	private Dimension northSideSize, southSideSize, eastSideSize, westSideSize;
	private ArrayList <Mail> listeMailRecherche;
	private JTextArea affichageMail;
	Utilisateur user;
	
	
	/**
	 * @AUTHOR selim
	 * METHODE RECHERCHE PAR MOT CLE
	 */
	public UiRecherche(String login) {
		
		super("CDA Messagerie");
		
		// create Layout
		setLayout(new BorderLayout());
		addWindowListener(this);
		
			
		//CREATE BORDER AND CENTRAL PANEL
		northSideSize = new Dimension (700, 30);
		southSideSize = new Dimension (700, 70);
		eastSideSize = new Dimension (25, 400);
		westSideSize = new Dimension (40, 400);
		
		northSpace = new JPanel();
		northSpace.setBackground(Color.WHITE);
		northSpace.setPreferredSize(northSideSize);
		
		westSpace = new JPanel();
		westSpace.setBackground(Color.WHITE);
		westSpace.setPreferredSize(westSideSize);
		
		eastSpace = new JPanel();
		eastSpace.setBackground(Color.WHITE);
		eastSpace.setPreferredSize(eastSideSize);
		
		southSpace = new JPanel();
		southSpace.setBackground(Color.WHITE);
		southSpace.setPreferredSize(southSideSize);
		
		central = new JPanel();
		central.setBackground(Color.WHITE);
		central.setLayout(new GridLayout(2,1));
		
		this.add(northSpace, BorderLayout.NORTH);
		this.add(westSpace, BorderLayout.WEST);
		this.add(eastSpace, BorderLayout.EAST);
		this.add(southSpace, BorderLayout.SOUTH);
		this.add(central, BorderLayout.CENTER);
		
		//BUILD CENTRAL PANEL
		centralNorth = new JPanel();
		centralNorth.setBackground(Color.WHITE);
		
		centralSouth = new JPanel();
		centralSouth.setBackground(Color.WHITE);
						
		centralNorth.setLayout(new GridLayout(4,1));
		centralSouth.setLayout(new GridLayout(1,1));
		
		
		//GET NAME USER AND SET TITLEDBORDER
		user = InstanceUser.recupererUser(login);
				
		welcome = new JLabel("Bonjour "+user.getPrenom()+" "+user.getNom());
		welcome.setFont(new Font("Bahnschrift", Font.BOLD, 30));
		welcome.setForeground(Color.GRAY);
		
		centralNorth.add(welcome);
				
		//ACCUEIL / DECONNEXION
		disconnectLine = new JPanel();
		disconnectLine.setBackground(Color.WHITE);
		disconnectLine.setLayout(new GridLayout(1,8));
		
		accueil = new JLabel("<HTML><U>Accueil</U></HTML>");
		accueil.setName("accueil");
		accueil.setForeground(Color.blue);
		accueil.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		accueil.addMouseListener(this);
		disconnectLine.add(accueil);
		
		deconnexion = new JLabel("<HTML><U>Déconnexion</U></HTML>");
		deconnexion.setName("deconnexion");
		deconnexion.setForeground(Color.blue);
		deconnexion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		deconnexion.addMouseListener(this);
		disconnectLine.add(deconnexion);
		
		disconnectLine.add(Box.createHorizontalGlue());
		disconnectLine.add(Box.createHorizontalGlue());
		disconnectLine.add(Box.createHorizontalGlue());
		disconnectLine.add(Box.createHorizontalGlue());
		disconnectLine.add(Box.createHorizontalGlue());
		disconnectLine.add(Box.createHorizontalGlue());
		
		centralNorth.add(disconnectLine);
				
		searchLine = new JPanel();
		searchLine.setBackground(Color.WHITE);;
		searchLine.setLayout(new GridLayout(1,5));
		
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		rechercher = new JTextField();
		rechercher.setBorder(border);
		rechercher.setEnabled(true);
		rechercher.addActionListener(this);
		searchLine.add(rechercher);
		
		search = new JLabel("   Rechercher  > ");
		search.setName("search");
		search.setBackground(Color.WHITE);
		search.setForeground(Color.black);
		search.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		search.addMouseListener(this);
		searchLine.add(search);
		
		searchLine.add(Box.createHorizontalGlue());
		searchLine.add(Box.createHorizontalGlue());
		searchLine.add(Box.createHorizontalGlue());
				
		centralNorth.add(searchLine);
		centralNorth.add(Box.createHorizontalGlue());
		
		central.add(centralNorth);
		
		affichageMail = new JTextArea();
		affichageMail.setBorder(border);
		affichageMail.setEditable(false);
		affichageMail.setVisible(true);
		centralSouth.add(affichageMail);
		central.add(centralSouth);
		
				
		pack();
		setVisible(true);
		setSize(700, 400);
		setLocation(450, 250);
		
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println(((JLabel)e.getSource()).getName());
		
		
				
		if (((JLabel)e.getSource()).getName().contentEquals("accueil")) {
			
			UiAccueil accueil = new UiAccueil();
		}
		
		if (((JLabel)e.getSource()).getName().contentEquals("deconnexion")) {
			
			UiAuthentification auth = new UiAuthentification();
		}
		
//		if (((JLabel)e.getSource()).getName().contentEquals("search")) {
//			
//			listeMailRecherche = communicationBdd.listeMail(rechercher.getText());
//			affichageMail.setText(listeMailRecherche.toString());
//		}
		
		if (((JLabel)e.getSource()).getName().contentEquals("search")) {
			
			listeMailRecherche = communicationBdd.listeMail(rechercher.getText());
			
			for (Mail mail : listeMailRecherche) {	
			affichageMail.setText(mail.toString());
			}
		}
		
		else { System.out.println("Probleme d'envoi");}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
