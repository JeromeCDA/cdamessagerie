package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import fr.afpa.control.ControlUser;

public class UiAuthentification extends JFrame implements ActionListener, WindowListener, TextListener, MouseListener {

	private JTextField login, password;
	private JLabel labLogin, labPassword, titreCDA;
	public static JLabel incorrectLogin;
	private JButton creation, connexion;
	private Dimension longSize, sideSize;

	/**
	 * @author selim 
	 * METHODE CREATION NOUVEAU MESSAGE
	 */
	public UiAuthentification() {

		super("CDA Messagerie");

		// create Layout
		setLayout(new BorderLayout());
		addWindowListener(this);

		// CREATE AND ADD BORDER AND CENTRAL PANEL
		longSize = new Dimension(700, 50);
		sideSize = new Dimension(50, 400);

		JPanel northSpace = new JPanel();
		northSpace.setPreferredSize(longSize);
		northSpace.setBackground(Color.WHITE);

		JPanel westSpace = new JPanel();
		westSpace.setPreferredSize(sideSize);
		westSpace.setBackground(Color.WHITE);

		JPanel eastSpace = new JPanel();
		eastSpace.setPreferredSize(sideSize);
		eastSpace.setBackground(Color.WHITE);

		JPanel southSpace = new JPanel();
		southSpace.setPreferredSize(longSize);
		southSpace.setBackground(Color.WHITE);

		JPanel central = new JPanel();
		central.setBackground(Color.WHITE);
		central.setLayout(new GridLayout(6, 1));

		this.add(northSpace, BorderLayout.NORTH);
		this.add(westSpace, BorderLayout.WEST);
		this.add(eastSpace, BorderLayout.EAST);
		this.add(southSpace, BorderLayout.SOUTH);
		this.add(central, BorderLayout.CENTER);

		// BUILD 6 LINES GRIDLAYOUT
		titreCDA = new JLabel("CDA 20156 - Messagerie", SwingConstants.CENTER);
		titreCDA.setFont(new Font("Bahnschrift", Font.BOLD, 20));
		titreCDA.setPreferredSize(new Dimension(300, 50));
		;
		central.add(titreCDA);

		incorrectLogin = new JLabel("Login / Mot de passe incorrect");
		incorrectLogin.setFont(new Font("Bahnschrift", Font.BOLD, 12));
		incorrectLogin.setForeground(Color.RED);
		incorrectLogin.setVisible(false);
		central.add(incorrectLogin);

		JPanel centralLogin1 = new JPanel();
		centralLogin1.setLayout(new FlowLayout());
		centralLogin1.setBackground(Color.WHITE);
		labLogin = new JLabel("Login ");
		labLogin.setPreferredSize(new Dimension(150, 20));
		login = new JTextField();
		login.addActionListener(this);
		login.setPreferredSize(new Dimension(120, 20));
		login.setEditable(true);
		centralLogin1.add(labLogin);
		centralLogin1.add(login);
		central.add(centralLogin1);

		JPanel centralLogin2 = new JPanel();
		centralLogin2.setLayout(new FlowLayout());
		centralLogin2.setBackground(Color.WHITE);
		labPassword = new JLabel("Mot de passe ");
		labPassword.setPreferredSize(new Dimension(150, 20));
		password = new JTextField();
		password.addActionListener(this);
		password.setPreferredSize(new Dimension(120, 20));
		password.setEditable(true);
		centralLogin2.add(labPassword, password);
		centralLogin2.add(password);
		central.add(centralLogin2);

		central.add(Box.createHorizontalGlue());

		JPanel connexionLine = new JPanel();
		connexionLine.setLayout(new FlowLayout());
		connexionLine.setBackground(Color.WHITE);
		creation = new JButton("Creation de compte");
		creation.setName("creation");
		creation.addMouseListener(this);
		connexion = new JButton("Connexion");
		connexion.setName("connexion");
		connexion.addMouseListener(this);
		connexionLine.add(creation);
		connexionLine.add(connexion);
		central.add(connexionLine);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(700, 400);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setVisible(true);
		
	}

	/**
	 * @author selim
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println(((JButton) e.getSource()).getName());

		ControlUser control = new ControlUser();

		if (((JButton) e.getSource()).getName().contentEquals("connexion")) {

			control.controlValidUser(login.getText(), password.getText());
			incorrectLogin.setVisible(false);

		} else if (((JButton) e.getSource()).getName().contentEquals("creation")) {
			this.setVisible(false);
			//Appelle la frame de création de compte si l'utilisateur clique sur le bouton "création de compte"
			UiCreationCompte crea = new UiCreationCompte();
		}

	}
	
	public static void incorrectLoginOrPassword() {
			JFrame f=new JFrame();
			JOptionPane.showMessageDialog(f, "Login / Mot de passe incorrect", "Alert",JOptionPane.WARNING_MESSAGE);
	}
	
	
	
	public static void emptyFields() {
		JFrame f=new JFrame();
		JOptionPane.showMessageDialog(f, "Veuillez renseigner les champs vides", "Alert",JOptionPane.WARNING_MESSAGE);
	}
	
	

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
