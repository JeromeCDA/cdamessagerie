package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;

public class UiAccueil extends JFrame implements ActionListener,WindowListener,MouseListener {
	
	private JMenuBar menuBar;
	private JMenu menuFile;
	private JMenu menuEdit;
	private JMenu menuHelp;
	private JMenuItem newMessage;
	private JMenuItem disconnect;
	private JMenuItem showAccount;
	private JMenuItem modifyAccount;
	private JLabel labelBonjour;
	private JLabel labelNom;
	private JLabel labelPrenom;
	
	public UiAccueil() {
		// TODO Auto-generated constructor stub
		
		JFrame window = new JFrame("CDA Messagerie");
		
		
		
		window.setLayout(new BorderLayout());
		
		
		
		//Creation du menu
		menuBar = new JMenuBar();
		
		menuFile = new JMenu("Fichier");
		newMessage = new JMenuItem("New Message");
		newMessage.addActionListener(this);
		disconnect = new JMenuItem("Deconnexion");
		disconnect.addActionListener(this);
		menuFile.add(newMessage);
		menuFile.add(disconnect);
		
		
		menuEdit = new JMenu("Edit");
		showAccount = new JMenuItem("Visualiser compte");
		showAccount.addActionListener(this);
		modifyAccount = new JMenuItem("Modifier compte");
		modifyAccount.addActionListener(this);
		menuEdit.add(showAccount);
		menuEdit.add(modifyAccount);
		
		
		menuHelp = new JMenu("Help");
		menuHelp.addActionListener(this);
		
		menuBar.add(menuFile);
		menuBar.add(menuEdit);
		menuBar.add(menuHelp);
		window.setJMenuBar(menuBar);
		
		
		//creation des Labels
		
		JPanel northPanel = new JPanel();
		FlowLayout northFl = new FlowLayout(FlowLayout.CENTER,10,20);
		northPanel.setLayout(northFl);
		window.add(northPanel,BorderLayout.NORTH);
		
		labelBonjour = new JLabel("Bonjour");
		labelBonjour.setFont(new Font("Roboto",Font.BOLD,18));
		labelBonjour.setForeground(Color.BLACK);
		northPanel.add(labelBonjour, BorderLayout.NORTH);
		
		labelNom = new JLabel("Lohez");
		labelNom.setFont(new Font("Roboto",Font.BOLD,18));
		labelNom.setForeground(Color.BLACK);
		northPanel.add(labelNom, BorderLayout.NORTH);
		
		labelPrenom = new JLabel("Christian");
		labelPrenom.setFont(new Font("Roboto",Font.BOLD,18));
		labelPrenom.setForeground(Color.BLACK);
		northPanel.add(labelPrenom, BorderLayout.NORTH);
		
		
		//Creation de la fenetre des mails et des onglets
		JPanel centerPanel = new JPanel();
		GridBagLayout centerGbl = new GridBagLayout();
		centerPanel.setLayout(centerGbl);
		window.add(centerPanel, BorderLayout.CENTER);
		
		//Creation des panneaux
		GridBagLayout mailGbl = new GridBagLayout();
		JPanel panelReceived = new JPanel();
		panelReceived.setLayout(mailGbl);
		JPanel panelSent = new JPanel();
		panelSent.setLayout(mailGbl);
		JPanel panelSearch = new JPanel();
		panelSearch.setLayout(mailGbl);
		
		//Créer le conteneur des onglets
	    JTabbedPane onglets = new JTabbedPane();
	    
	    //Définir la position de conteneur d'onglets
	    onglets.setPreferredSize(new Dimension(600, 200));
	    
	    //Associer chaque panneau à l'onglet correspondant
	    onglets.add("Boite de reception", panelReceived);
	    onglets.add("Message envoyés", panelSent);
	    onglets.add("Recherche de mail", panelSearch);
	    
	    //Ajout des cadres contenant les mails
	    JTextPane mailsReceived = new JTextPane();
	    panelReceived.add(mailsReceived);
	    mailsReceived.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	    mailsReceived.setPreferredSize(new Dimension(500, 100));
	    
	    JTextPane mailsSent = new JTextPane();
	    panelSent.add(mailsSent);
	    mailsSent.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	    mailsSent.setPreferredSize(new Dimension(500, 100));
	    
	    JTextPane mailsSearch = new JTextPane();
	    panelSearch.add(mailsSearch);
	    mailsSearch.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	    mailsSearch.setPreferredSize(new Dimension(500, 100));
	    
	    //Ajouter les onglets au centerPanel
	    centerPanel.add(onglets);
	    
		
		
		window.setSize(700, 400);
		window.setLocationRelativeTo(null);
		window.setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println();
		if(((JMenuItem) e.getSource()).getText().equals("New Message")) {
			UiNouveauMessage newMessage = new UiNouveauMessage();
			
		}else if(((JMenuItem) e.getSource()).getText().equals("Visualiser compte")) {
			System.out.println(((JMenuItem) e.getSource()).getText());
			//System.exit(0);
			
		}else if(((JMenuItem) e.getSource()).getText().equals("Modifier compte")) {
			System.out.println(((JMenuItem) e.getSource()).getText());
			//System.exit(0);
			
		}else if(((JMenu) e.getSource()).getName().equals("Help")) {
			
			System.out.println(((JMenuItem) e.getSource()).getName());
			//ServiceMessagePopUp.menuHelp();
			
		}else if(((JMenuItem) e.getSource()).getText().equals("Deconnexion")) {
			
			System.exit(0);
			
		}
	}

}
