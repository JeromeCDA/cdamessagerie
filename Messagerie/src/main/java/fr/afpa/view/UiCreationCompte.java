package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;

import fr.afpa.control.ControlUser;

public class UiCreationCompte extends JFrame implements ActionListener, TextListener, WindowListener, FocusListener, MouseListener {
	
	
	private JPanel mainPanel, innerPanel, eastPanel, westPanel, northPanel, southPanel, emptyCell, borderPanel;
	private JButton validerButton;
	private JLabel nom, prenom, email, telnum, login, mdp, empty, format;
	private JTextField nomField, prenomField, emailField, telnumField, loginField;
	private JPasswordField hidenPasswordField;
	private Box horizontalBox1, horizontalBox2, horizontalBox3, horizontalBox4, horizontalBox5, horizontalBox6;
	private Container contentPane;
	private Border innerBorder, outerBorder;
	
	
	public UiCreationCompte() {
		
		//Panel avec bordure visible avec du padding afin de l'isoler au centre
		this.setTitle("CDA Messagerie");
		mainPanel =  new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		nom = new JLabel("Nom :");
		prenom = new JLabel("Prénom :");
		email = new JLabel("Mail :");
		telnum = new JLabel("Num téléphone :");
		login = new JLabel("Login :");
		mdp = new JLabel("Mot de passe :");
		
		format = new JLabel("8 caractères, chiffres ou lettres");
		
		nomField = new JTextField();
		prenomField = new JTextField();
		emailField = new JTextField();
		telnumField = new JTextField();
		loginField = new JTextField();
		
		hidenPasswordField = new JPasswordField();
		
		validerButton = new JButton("Valider");
		validerButton.setName("Valider");
		validerButton.addMouseListener(this);
		


		
		
		//Panel intérieur afin de faire des marges pour ne pas coller les éléments internes à la bordure du panneau visible (mainPanel)
		innerPanel = new JPanel();
		innerPanel.setBorder(BorderFactory.createTitledBorder("Création de compte : "));
		

		innerPanel.setLayout(new GridLayout(6,3));
		

		
		innerPanel.add(nom);
		innerPanel.add(nomField);
		innerPanel.add(Box.createHorizontalGlue());
		
		innerPanel.add(prenom);
		innerPanel.add(prenomField);
		innerPanel.add(Box.createHorizontalGlue());
		
		innerPanel.add(email);
		innerPanel.add(emailField);
		innerPanel.add(Box.createHorizontalGlue());
		
		innerPanel.add(telnum);
		innerPanel.add(telnumField);
		innerPanel.add(Box.createHorizontalGlue());
		
		innerPanel.add(login);
		innerPanel.add(loginField);
		innerPanel.add(Box.createHorizontalGlue());
		
		innerPanel.add(mdp);
		innerPanel.add(hidenPasswordField);
		innerPanel.add(format);
		
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(5,1));
		panel.add(new JLabel("                    "));
		panel.add(new JLabel("                    "));
		panel.add(new JLabel("                    "));
		panel.add(new JLabel("                    "));
		panel.add(new JLabel("                    "));

		
		JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayout(5,1));
		panel3.add(new JLabel("                    "));
		panel3.add(new JLabel("                    "));
		panel3.add(new JLabel("                    "));
		panel3.add(new JLabel("                    "));
		panel3.add(new JLabel("                    "));
		
		JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayout(5,1));
		panel4.add(new JLabel("                    "));
		panel4.add(new JLabel("                    "));
		panel4.add(new JLabel("                    "));
		panel4.add(new JLabel("                    "));
		panel4.add(new JLabel("                    "));
		
		
		JPanel panelButton = new JPanel();
		panelButton.setLayout(new GridLayout(1,3));
		
		panelButton.add(Box.createHorizontalGlue());
		panelButton.add(Box.createHorizontalGlue());
		panelButton.add(validerButton);
		
		mainPanel.add(panel, BorderLayout.NORTH);
		mainPanel.add(panelButton, BorderLayout.SOUTH);
		mainPanel.add(panel3, BorderLayout.EAST);
		mainPanel.add(panel4, BorderLayout.WEST);
		
		
		panelButton.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));  
		
		
		mainPanel.add(innerPanel, BorderLayout.CENTER);
		this.add(mainPanel);
		
		this.addWindowListener(this);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(700, 400);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		this.setVisible(true);
		

	}
	
	/**@author Djé
	 * Si la source du bouton est égale au nom du bouton set au préalable dans la construction de la frame, appliquer les instructions qui suivent, donc récupérer tous les textes des champs et les mettre
	 * en paramètres de la méthode générale de contrôle de la classe UiCreationCompte
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		ControlUser cu = new ControlUser();
		System.out.println(((JButton)e.getSource()).getName());
		
		if(((JButton)e.getSource()).getName().equals("Valider")) {
			String password = String.valueOf(hidenPasswordField.getPassword());
//			System.out.println(password);
			cu.controlOkForInstanceUser(nomField.getText(), prenomField.getText(), loginField.getText(), emailField.getText(), telnumField.getText(), password);
			this.setVisible(false);
		}
	}
	
	/**@author Djé
	 * Méthode qui alerte sous la forme d'un pop-up que le format du mot de passe n'est pas respecté
	 */
	public void passwordWrongFormat() {
		JFrame f=new JFrame();
		JOptionPane.showMessageDialog(f, "Votre mot de passe doit faire 8 caractères, chiffres ou lettres", "Alert",JOptionPane.WARNING_MESSAGE);
	}
	
	/**@author Djé
	 * Méthode qui alerte sous la forme d'un pop-up qu'un des champs d'inscription n'est pas rempli
	 */
	public void emptyFields() {
		JFrame f=new JFrame();
		JOptionPane.showMessageDialog(f, "Tous les champs doivent être remplis", "Alert",JOptionPane.WARNING_MESSAGE);
	}
	
	/**@author Djé
	 * Méthode qui alerte sous la forme d'un pop-up que le login ou le mail saisi est déjà utilisé dans la DB
	 */
	public void sqlUniqueException() {
		JFrame f=new JFrame();
		JOptionPane.showMessageDialog(f, "Le login ou l'email existe déjà", "Alert",JOptionPane.WARNING_MESSAGE);
	}
	
	
	/**@author Djé
	 * Méthode générique, adaptable et exploitable par plusieurs classes UI qui permet de créer une pop-up de réussite d'un scénario utilisateur et de switch vers la frame souhaitée
	 */
	public void switchToAuthentification() {
		this.setVisible(false);
		JFrame f = new JFrame();
		Object[] options = {"Retour à la page d'authentification"};
		int option = JOptionPane.showOptionDialog(null,
                "Votre compte a été créé avec succès,",
                "Création de compte : ",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
		if (option == JOptionPane.OK_OPTION) {
			//appelle la frame d'authentification si le compte est bien créé
			UiAuthentification authen = new UiAuthentification();
		}
	}
	

	@Override
	public void focusGained(FocusEvent e) {
		
	}

	
	@Override
	public void focusLost(FocusEvent e) {
		
	}

	
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textValueChanged(TextEvent e) {
				
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseEntered(MouseEvent arg0) {

	}



	@Override
	public void mouseExited(MouseEvent arg0) {

	}



	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
